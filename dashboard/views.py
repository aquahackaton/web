from django.shortcuts import render, HttpResponse, redirect, HttpResponseRedirect, get_object_or_404
from django.utils import timezone
from django.http import JsonResponse, HttpResponseNotFound
from .forms import *
from core.models import *
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required(login_url = "login")
def dashboard_index(request):
    if request.method == 'POST':
        id = request.POST.get("id")
        close = request.POST.get("close")
        id = int(id)
        counter = Counter.objects.filter(id=id).first()
        counter.close = True if close == "0" else False
        counter.save()
    users = User.objects.filter(is_superuser=False,is_staff=False)
    data = []
    for u in users:
        m = {}
        m['username']=u.username
        m['first_name']=u.first_name
        m['last_name']=u.last_name
        counter = Counter.objects.filter(user=u).first()
        last = LastData.objects.filter(counter=counter).first()
        indicator = last.indicator if last else 0
        m['counter']=counter.imei
        m['indicator'] = indicator
        counterdata = CounterData.objects.filter(counter=counter).first()
        if counterdata:
            usage = counterdata.usage
        else:
            usage = 0.0
        m['usage']= round(indicator * counter.tarif, 2)
        m['close']=counter.close
        m['id']=counter.id
        data.append(m)
    return render(request,"dashboard/index.html",{'data':data})