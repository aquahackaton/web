from django import forms 
from .models import User
from django.utils.translation import ugettext_lazy as _
# from django.contrib.auth.models import User


class LoginForm(forms.ModelForm):
    username=forms.CharField(widget=forms.TextInput(
        attrs = {
            'class': 'form-control',
            'id'   : 'username',
            'name' : 'username',
        }
    ))
    password=forms.CharField(widget=forms.PasswordInput(
        attrs = {
            'class': 'form-control',
            'id' : 'password',
            'name' : 'password',
        }
    ))

    def clean(self):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")
        values = {
            'username' : username,
            'password' : password,
        }
        return values
    class Meta:
        model = User
        fields = ('username', 'password',)
        widgets = {
            'password': forms.PasswordInput(),
        }