from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from .models import Data, Counter, Report
from datetime import date



@receiver(post_save, sender=Data)
def update_report(sender, instance=None, created=False, **kwargs):
	if created:
		today = date.today()
		report = Report.objects.select_related('counter').filter(counter=instance.counter,created_at__gte=today).first()

		if report is None:
			report = Report(counter=instance.counter)

		indicator = round(instance.indicator - Data.objects.filter(counter=instance.counter,created_at__gte=today).order_by('created_at').first().indicator,3)

		report.indicator_usage = indicator

		report.limit = instance.counter.limit if instance.counter.limit else 0

		report.save()