from django.shortcuts import render, HttpResponse, redirect, HttpResponseRedirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.http import JsonResponse, HttpResponseNotFound
from django.contrib.auth import authenticate,logout
from django.contrib.auth import login as user_login
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .models import *
from .forms import *

from datetime import date, timedelta

# Create your views here.

def login(request):
    if request.user.is_authenticated:
        if request.user.is_superuser or request.user.is_staff:
            return redirect('dashboard_index')
        return redirect('index')
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            data = form.clean()
            username = data["username"]
            password = data["password"]
            user = authenticate(request, username=username, password=password)
            if user:
                user_login(request,user)
                request.session.set_expiry(60*60*24*365)
                if user.is_superuser or user.is_staff:
                    return redirect('dashboard_index')
                return redirect('index')
            messages.info(request,"Isdifadəçi adı və ya Parol səhvdir")
            return render(request,"login.html", {'form': form})
        messages.info(request,"Isdifadəçi adı və ya Parol düzgün qeyd olunmuyub")
        return render(request,"login.html", {'form': form})
    form = LoginForm()
    return render(request,"login.html", {'form': form})

def logoutUser(request):
    logout(request)
    messages.success(request,"Çıxış edildi")
    return redirect("login")


@csrf_exempt
def post_data(request):
    if request.method == 'GET':
        return HttpResponse("GET METODU ISDIFADE OLUNUB", status=404)
    req_data = request.body.decode().strip()
    try:
        req_data = req_data.split(',')
    except:
        return HttpResponse("DATA PARSE OLUNMUR", status=404)
    try:
        imei = req_data[0].strip()
    except:
        return HttpResponse("DATA DUZGUN DEYIL", status=404)
    try:
        indicator = req_data[1].strip()
    except:
        return HttpResponse("DATA DUZGUN DEYIL", status=404)
    try:
        counter = Counter.objects.get(imei=imei)
    except:
        return HttpResponse("CIHAZ SERVERDE TAPILMADI", status=404)
    indicator = round(float(indicator)/1000, 3)
    
    Data.objects.create(counter=counter, indicator=indicator)
    
    last = LastData.objects.filter(counter=counter).first()
    
    if last:
        usage = round((indicator - last.indicator) * counter.tarif, 3)
        last.indicator = indicator
        last.save()
    else:
        LastData.objects.create(counter=counter, indicator=indicator)
        usage = 0
    
    counterdata = CounterData.objects.filter(counter=counter).first()
    
    if counterdata:
        counterdata.usage += usage
        counterdata.save()
    else:
        CounterData.objects.create(counter=counter, usage=usage)
    
    send = 1 if counter.close else 0
    return HttpResponse(send)

@login_required(login_url = "login")
def index(request):
    if request.user.is_superuser or request.user.is_staff:
        return redirect('dashboard_index')
    
    counter = Counter.objects.filter(user=request.user).first()
    
    last = LastData.objects.filter(counter=counter).first()
    counterdata = CounterData.objects.filter(counter=counter).first()
    today = date.today()
    indicator = None
    this_month = None
    this_day = None
    history = {}
    if last:
        this_month = round(last.indicator - Data.objects.filter(
            counter=counter,
            created_at__year = today.year,
            created_at__month = today.month).order_by('created_at').first().indicator,3)
        this_day = round(Report.objects.filter(counter=counter).order_by('-created_at').first().indicator_usage, 3)
        indicator = round(last.indicator,3)
    week_ago = today - timedelta(days=7)
    
    weekly_report = Report.objects.filter(counter=counter,created_at__gte=week_ago).all()
    
    usage = round((last.indicator - Data.objects.filter(counter=counter).order_by('created_at').first().indicator) * counter.tarif,2)
    
    context = {
        'counter': counter,
        'indicator':indicator,
        'today': this_day,
        'this_month': this_month,
        'usage':usage,
        'weekly_report': weekly_report,
    }
    
    return render(request, "index.html", context)