from django.contrib import admin

from django.contrib.auth.admin import UserAdmin

# Register your models here.

from .models import *

class CounterAdmin(admin.ModelAdmin):
	list_display = ['user', 'imei', 'tarif', 'adress', 'created_at']
	actions = ["make_delete"]

class DataAdmin(admin.ModelAdmin):
	list_display = ['counter', 'indicator', 'created_at']
	actions = ["make_delete"]
	list_filter = ('counter',)

class LastDataAdmin(admin.ModelAdmin):
	list_display = ['counter', 'indicator', 'created_at', 'updated_at']
	actions = ["make_delete"]
	list_filter = ('counter',)

class CounterDataAdmin(admin.ModelAdmin):
	list_display = ['counter', 'usage', 'created_at', 'updated_at']
	actions = ["make_delete"]

class ReportAdmin(admin.ModelAdmin):
	list_display=['counter', 'indicator_usage', 'created_at', 'updated_at']
	actions = ["make_delete"]

admin.site.register(User, UserAdmin)
admin.site.register(Counter, CounterAdmin)
admin.site.register(Data, DataAdmin)
admin.site.register(LastData, LastDataAdmin)
admin.site.register(CounterData, CounterDataAdmin)
admin.site.register(Report, ReportAdmin)