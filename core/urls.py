from django.urls import path
from .views import *


urlpatterns = [
	path('', index, name="index"),
	path('login/',login, name="login"),
    path('logout/', logoutUser, name='logout'),
    path('a/s/t/m/data/', post_data),
]