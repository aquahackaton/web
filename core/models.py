from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator
from django.contrib.auth.models import AbstractUser

# Create your models here.

class User(AbstractUser):
	def __str__(self):
		if self.first_name or self.last_name:
			return self.first_name + " " + self.last_name
		else:
			return self.username
	def full_name(self):
		if self.first_name or self.last_name:
			return self.first_name + " " + self.last_name
		else:
			return self.username

class Counter(models.Model):
	imei = models.CharField(_("Abonent kodu"), max_length=255, unique=True)
	user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='user_counter')
	tarif = models.FloatField(_("Tarif"))
	adress=models.CharField(_("Ünvan"), max_length=255, null=True, blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	limit = models.FloatField(_("Limit"), default=10)
	close=models.BooleanField(_("Klapani Aç/Bağla"), default=False)

	def __str__(self):
		return self.imei

	class Meta:
		verbose_name="Sayğac"
		verbose_name_plural='Sayğac'

class Data(models.Model):
	counter = models.ForeignKey(Counter, on_delete = models.CASCADE, related_name='counter_data')
	indicator = models.FloatField(_("Göstərici"))
	created_at = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return str(self.indicator)

	class Meta:
		ordering = ['-created_at']
		verbose_name="Sayğac Datası"
		verbose_name_plural='Sayğac Datası'


class LastData(models.Model):
	counter = models.ForeignKey(Counter, on_delete = models.CASCADE, related_name='counter_last_data')
	indicator = models.FloatField(_("Göstərici"))
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return str(self.indicator)
	class Meta:
		verbose_name="Sayğac Axırıncı Datası"
		verbose_name_plural='Sayğac Axırıncı Datası'

class CounterData(models.Model):
	counter = models.OneToOneField(Counter, on_delete=models.CASCADE, related_name='_counter')
	usage=models.FloatField(_("Sərfiyyat"))
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name='Sayğac məlumatı'
		verbose_name_plural='Sayğac məlumatı'

	def __str__(self):
		return str(self.usage)

	def get_dept(self):
		return self.usage * self.counter.tarif


class Report(models.Model):
	counter=models.ForeignKey(Counter, on_delete=models.CASCADE, related_name='counter_report')
	indicator_usage=models.FloatField(_("Göstərici"))
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	limit = models.FloatField(_("Limit"), null=True, blank=True)

	def __str__(self):
		return str(self.indicator_usage)

	def usage(self):
		return self.indicator_usage * self.counter.tarif
